#include "pch.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define sdl_perror(desc) do {\
    fmt::print(stderr, "{}: {}\n", (desc), SDL_GetError()); \
    exit(1); \
} while(0)

constexpr int SCREEN_WIDTH = 800;
constexpr int SCREEN_HEIGHT = 600;

struct Pixel {
    uint8_t r;
    uint8_t g;
    uint8_t b;

    bool operator == (const Pixel& o) const {
        return r == o.r && g == o.g && b == o.b;
    }
} __attribute__((packed));

template<>
struct std::hash<Pixel> {
    std::size_t operator()(const Pixel& p) const {
        size_t num = (p.b << 16) | (p.g << 8) | p.r;
        return hash<size_t>()(num);
    }
};

template<typename T>
struct Vector2D {
    T x;
    T y;

    template<typename U>
    Vector2D<T> operator+(Vector2D<U> o) {
        return { x + o.x, y + o.y };
    }

    template<typename U>
    Vector2D<T> operator-(Vector2D<U> o) {
        return { x - o.x, y - o.y };
    }

    Vector2D(T x, T y) : x(x), y(y) { }

    template<typename U>
    Vector2D(Vector2D<U> o) : x((T)o.x), y((T)o.y) { }
};

template<typename T>
struct fmt::formatter<Vector2D<T>> {
    template<typename FormatContext>
    auto format(Vector2D<T> const& vec, FormatContext& ctx) {
        return fmt::format_to(ctx.out(), "({}, {})", vec.x, vec.y);
    }

    template<typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {
        return ctx.begin();
    }
};

template<>
struct fmt::formatter<Pixel> {
    template<typename FormatContext>
    auto format(Pixel const& p, FormatContext& ctx) {
        return fmt::format_to(ctx.out(), "({} {} {})", p.r, p.g, p.b);
    }

    template<typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {
        return ctx.begin();
    }
};

template<>
struct fmt::formatter<SDL_Rect> {
    template<typename FormatContext>
    auto format(SDL_Rect const& r, FormatContext& ctx) {
        return fmt::format_to(ctx.out(), "Rect ({} {} {})", r.x, r.y, r.w, r.h);
    }

    template<typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {
        return ctx.begin();
    }
};

template<typename T, typename U>
Vector2D<U> operator*(U m, Vector2D<T> v) {
    return { v.x*m, v.y*m };
}

template<typename T, typename U>
Vector2D<U> operator/(Vector2D<T> v, U m) {
    return { v.x/m, v.y/m };
}

class ImageViewport {
public:
    ImageViewport(size_t image_width, size_t image_height, uint32_t viewport_width, uint32_t viewport_height)
        : m_image_size { image_width, image_height }, m_viewport_size { viewport_width, viewport_height }
        {}
    ~ImageViewport() = default;

    void mouse_down(uint32_t x, uint32_t y);
    void mouse_up(uint32_t x, uint32_t y);
    void mouse_move(uint32_t x, uint32_t y);
    void mouse_wheel(double wheel_delta);

    bool is_dragging();

    void tick();

    std::optional<Vector2D<size_t>> current_location_in_imagespace();

    SDL_Rect image_rect() const;
    SDL_Rect viewport_rect() const;
private:
    Vector2D<size_t> m_image_size;
    Vector2D<uint32_t> m_viewport_size;

    bool m_is_mouse_down { false };
    Vector2D<int32_t> m_last_click_pos { 0, 0 };
    Vector2D<int32_t> m_current_cursor_pos { 0, 0 };
    
    Vector2D<double> m_curr_offset { 0., 0. };
    double m_zoom_level { 1. };
};

void ImageViewport::mouse_down(uint32_t x, uint32_t y) {
    m_is_mouse_down = true;

    Vector2D<uint32_t> pos { x, y };
    m_current_cursor_pos = pos;
    m_last_click_pos = pos;
}

void ImageViewport::mouse_move(uint32_t x, uint32_t y) {
    Vector2D<uint32_t> pos { x, y };
    m_current_cursor_pos = pos;
}

void ImageViewport::mouse_up(uint32_t x, uint32_t y) {
    m_is_mouse_down = false;

    Vector2D<uint32_t> pos { x, y };
    m_current_cursor_pos = pos;
}

void ImageViewport::mouse_wheel(double wheel_delta) {
    double factor = 100.f;
    double old_zoom_level = m_zoom_level;
    m_zoom_level *= (1.f + wheel_delta/factor);

    Vector2D<double> cursor_location = m_current_cursor_pos;

    // the image coordinates under cursor before the zoom
    Vector2D<double> image_under_cursor = cursor_location / old_zoom_level + m_curr_offset;
    Vector2D<double> new_offset = image_under_cursor - cursor_location / m_zoom_level;

    m_curr_offset = new_offset;
}

void ImageViewport::tick() {
    if(!m_is_mouse_down) return;

    double delta = 2.0f / (m_zoom_level * m_zoom_level);

    Vector2D<double> distance_to_click = m_current_cursor_pos - m_last_click_pos;

    double len = sqrt(distance_to_click.x * distance_to_click.x + distance_to_click.y * distance_to_click.y);
    if(len == 0) return;

    distance_to_click = (delta/len) * distance_to_click;

    m_curr_offset = m_curr_offset + distance_to_click;
}

SDL_Rect ImageViewport::image_rect() const {
    double zoomed_width = m_viewport_size.x / m_zoom_level;
    double zoomed_height = m_viewport_size.y / m_zoom_level;

    return { (int)m_curr_offset.x, (int)m_curr_offset.y, (int)zoomed_width, (int)zoomed_height };
}

SDL_Rect ImageViewport::viewport_rect() const {    
    return { 0, 0, (int)m_viewport_size.x, (int)m_viewport_size.y };
}

std::optional<Vector2D<size_t>> ImageViewport::current_location_in_imagespace() {
    Vector2D<double> cursor_location = m_current_cursor_pos;
    Vector2D<double> res = cursor_location / m_zoom_level + m_curr_offset;

    if(res.x < 0 || res.y < 0) return {};
    if(res.x > m_image_size.x || res.y > m_image_size.y) return {};

    return (Vector2D<size_t>)res;
}

bool ImageViewport::is_dragging() {
    if(!m_is_mouse_down) return false;

    Vector2D<int32_t> d = m_current_cursor_pos - m_last_click_pos;
    size_t l_sq = d.x*d.x + d.y*d.y;

    // ignore if dragged length was less than 5px
    if(l_sq < 25) return false;
    return true;
}

// This class implements the naive median cut algorithm for color quantization
class ColorQuantizer {
public:
    ColorQuantizer(Pixel* data, size_t len);

    void quantize();
    Pixel color_at(size_t idx) const;
private:
    struct Item {
        size_t idx;
        Pixel color;
    };

    void quantization_step(size_t start, size_t end, int levels_left);

    void start_reporting(int levels);
    void report_step();
    void end_reporting(float seconds);

    std::vector<Item> pixels;
    int completed_steps = 0;
    int total_steps = 0;
};

ColorQuantizer::ColorQuantizer(Pixel* data, size_t len) {
    pixels.reserve(len);
    for(size_t i=0; i<len; i++) 
        pixels.push_back(Item {i, data[i]});
}

void ColorQuantizer::quantization_step(size_t start, size_t end, int levels_left) {    
    if(!levels_left) {
        int r_avg = 0;
        int g_avg = 0;
        int b_avg = 0;

        for(size_t i=start; i<end; i++) {
            Item& pixel = pixels[i];

            r_avg += pixel.color.r;
            g_avg += pixel.color.g;
            b_avg += pixel.color.b;
        }

        size_t count = end - start;
        r_avg /= count;
        g_avg /= count;
        b_avg /= count;

        Pixel avg_color { (uint8_t)r_avg, (uint8_t)g_avg, (uint8_t)b_avg };

        for(size_t i=start; i<end; i++) {
            Item& pixel = pixels[i];
            pixel.color = avg_color;
        }

        report_step();
        return;
    }

    Pixel min_values { 0xff, 0xff, 0xff };
    Pixel max_values { 0, 0, 0 };

    for(size_t i=start; i<end; i++) {
        Item& pixel = pixels[i];

        if(pixel.color.r < min_values.r) min_values.r = pixel.color.r;
        if(pixel.color.g < min_values.g) min_values.g = pixel.color.g;
        if(pixel.color.b < min_values.b) min_values.b = pixel.color.b;

        if(pixel.color.r > max_values.r) max_values.r = pixel.color.r;
        if(pixel.color.g > max_values.g) max_values.g = pixel.color.g;
        if(pixel.color.b > max_values.b) max_values.b = pixel.color.b;
    }

    int r_diff = max_values.r - min_values.r;
    int g_diff = max_values.g - min_values.g;
    int b_diff = max_values.b - min_values.b;

    std::function<uint8_t(Pixel)> get_color;
    if(r_diff > g_diff && r_diff > b_diff)
        get_color = [](Pixel p) { return p.r; };
    else if(g_diff > r_diff && g_diff > b_diff)
        get_color = [](Pixel p) { return p.g; };
    else
        get_color = [](Pixel p) { return p.b; };

    size_t mid = (start + end)/2;
    auto start_it = pixels.begin()+start;
    auto mid_it = pixels.begin()+mid;
    auto end_it = pixels.begin()+end;

    std::nth_element(start_it, mid_it, end_it, [&get_color](Item& a, Item& b) {
        return get_color(a.color) < get_color(b.color);
    });

    report_step();

    levels_left--;
    quantization_step(start, mid, levels_left);
    quantization_step(mid, end, levels_left);
}

void ColorQuantizer::start_reporting(int levels) {
    completed_steps = 0;
    total_steps = 1 << (levels+1);
    total_steps--;

    fmt::print("Quantization: Step {}/{} done", completed_steps, total_steps);
    fflush(stdout);
}

void ColorQuantizer::report_step() {
    completed_steps++;
    fmt::print("\rQuantization: Step {}/{} done", completed_steps, total_steps);
    fflush(stdout);
}

void ColorQuantizer::end_reporting(float seconds) {
    fmt::print("\nTotal time: {:.2f} seconds\n", seconds);
    fflush(stdout);
}

void ColorQuantizer::quantize() {
    int target_level = 6;
    size_t target_colors = 1 << target_level;

    std::unordered_set<Pixel> unique_colors;
    for(const Item& item : pixels) {
        unique_colors.insert(item.color);
    }

    // we don't have to do anything
    if(unique_colors.size() <= target_colors) return;

    auto start = std::chrono::system_clock::now();
    start_reporting(6);

    quantization_step(0, pixels.size(), 6);
    auto end = std::chrono::system_clock::now();

    std::sort(pixels.begin(), pixels.end(), [](Item& a, Item& b) {
        return a.idx < b.idx;
    });

    std::chrono::duration<float> elapsed = end-start;

    end_reporting(elapsed.count());
}

Pixel ColorQuantizer::color_at(size_t i) const {
    return pixels[i].color;
}

class GaussianBlur {
public:
    GaussianBlur(float sigma);

    void apply_in_place(size_t width, size_t height, Pixel* pixels);
private:
    float coeff_for_offset(int dx, int dy);
    // {prev_row, curr_row, next_row}
    Pixel apply_convolution(Pixel* rows[3]);

    float m_sigma;
    float m_coeff[9];
};

GaussianBlur::GaussianBlur(float sigma)
    : m_sigma(sigma) {
    for(int dy=-1; dy<=1; dy++) {
        for(int dx=-1; dx<=1; dx++) {
            m_coeff[3*(dy+1)+(dx+1)] = coeff_for_offset(dx, dy);
        }
    }

    float total = 0;
    for(size_t i=0; i<9; i++) total += m_coeff[i];
    for(size_t i=0; i<9; i++) m_coeff[i] /= total;
}

float GaussianBlur::coeff_for_offset(int dx, int dy) {
    float var = m_sigma*m_sigma;
    float coeff = 1.0f/(2*M_PI*var);
    float expo = exp(-(dx*dx+dy*dy)/(2*var));
    return coeff * expo;
}

Pixel GaussianBlur::apply_convolution(Pixel* rows[3]) {
    float r = 0.0;
    float g = 0.0;
    float b = 0.0;

    for(size_t i=0; i<9; i++) {
        r += m_coeff[i] * rows[i/3][i%3].r;
        g += m_coeff[i] * rows[i/3][i%3].g;
        b += m_coeff[i] * rows[i/3][i%3].b;
    }

    return Pixel { (uint8_t)r, (uint8_t)g, (uint8_t)b };
}

void GaussianBlur::apply_in_place(size_t width, size_t height, Pixel* pixels) {
    auto from_coords = [&](size_t x, size_t y) -> size_t {
        return y*width+x;
    };

    Pixel* output = new Pixel[height*width];

    Pixel* rows[3];

    for(size_t y=1; y<height-1; y++) {
        rows[0] = &pixels[from_coords(0, y-1)];
        rows[1] = &pixels[from_coords(0, y)];
        rows[2] = &pixels[from_coords(0, y+1)];

        for(size_t x=1; x<width-1; x++) {
            output[from_coords(x, y)] = apply_convolution(rows);

            rows[0]++;
            rows[1]++;
            rows[2]++;
        }
    }

    for(size_t x=0; x<width; x++) {
        output[from_coords(x, 0)] = pixels[from_coords(x, 0)];
        output[from_coords(x, height-1)] = pixels[from_coords(x, height-1)];
    }

    for(size_t y=0; y<height; y++) {
        output[from_coords(0, y)] = pixels[from_coords(0, y)];
        output[from_coords(width-1, y)] = pixels[from_coords(width-1, y)];
    }

    memcpy(pixels, output, width*height*sizeof(Pixel));

    delete[] output;
}

class FloodableImage {
public:
    FloodableImage(size_t width, size_t height, const Pixel* orig_image);
    ~FloodableImage();

    Pixel original_color_at(Vector2D<size_t> coords);
    void flood_at(Vector2D<size_t> coords, Pixel fill_color);

    bool already_filled(Vector2D<size_t> coords);

    SDL_Surface* create_sdl_surface();
private:
    size_t m_width;
    size_t m_height;
    const Pixel* m_orig_image;
    Pixel* m_image;

    SDL_Surface* m_surface = nullptr;
};

FloodableImage::FloodableImage(size_t width, size_t height, const Pixel* orig_image)
    : m_width(width), m_height(height), m_orig_image(orig_image) {
    Pixel white { 0xff, 0xff, 0xff };
    size_t size = m_width*m_height;

    m_image = new Pixel[size];
    for(size_t i=0; i<size; i++) m_image[i] = white;
}

FloodableImage::~FloodableImage() {
    if(m_surface) SDL_FreeSurface(m_surface);
    delete[] m_image;
}

SDL_Surface* FloodableImage::create_sdl_surface() {
    assert(!m_surface);
    m_surface = SDL_CreateRGBSurfaceFrom((void*)m_image, m_width, m_height, 
        24, 3*m_width, 
        0x000000ff, 0x0000ff00, 0x00ff0000, 0x00000000);
    if(!m_surface) sdl_perror("SDL_CreateRGBSurfaceFrom");
    
    return m_surface;
}

void FloodableImage::flood_at(Vector2D<size_t> coords, Pixel fill_color) {
    assert(m_surface);
    if(SDL_LockSurface(m_surface) < 0) 
        sdl_perror("SDL_LockSurface");

    std::unordered_set<int> visited;
    std::queue<int> Q;

    auto orig_color = original_color_at(coords);
    // fmt::print("coords {} orig_color {}\n", coords, orig_color);

    auto from_coords = [this](Vector2D<int> p) -> std::optional<size_t> {
        if(p.x < 0 || p.y < 0) return {};
        if(p.x >= (int)m_width || p.y >= (int)m_height) return {};
        return p.y*m_width+p.x;
    };

    auto to_coords = [this](size_t idx) -> Vector2D<int> {
        return { (int)(idx % m_width), (int)(idx / m_width) };
    };

    auto index_has_orig_color = [this, &orig_color](size_t idx) -> bool {
        return m_orig_image[idx] == orig_color;
    };

    Q.push(coords.y*m_width + coords.x);

    while (!Q.empty()) {
        auto idx = Q.front();
        auto c = to_coords(idx);
        // fmt::print("visiting idx {} coords {}, queue size {}\n", idx, c, Q.size());
        Q.pop();

        if(visited.find(idx) != visited.end()) continue;
        visited.insert(idx);

        m_image[idx] = fill_color;

        auto left = from_coords({ c.x-1, c.y });
        auto right = from_coords({ c.x+1, c.y });
        auto top = from_coords({ c.x, c.y-1 });
        auto bottom = from_coords({ c.x, c.y+1 });

        if(left && index_has_orig_color(*left) && visited.find(*left) == visited.end())
            Q.push(*left);
        if(right && index_has_orig_color(*right) && visited.find(*right) == visited.end())
            Q.push(*right);
        if(top && index_has_orig_color(*top) && visited.find(*top) == visited.end())
            Q.push(*top);
        if(bottom && index_has_orig_color(*bottom) && visited.find(*bottom) == visited.end())
            Q.push(*bottom);
    }

    SDL_UnlockSurface(m_surface);
}

Pixel FloodableImage::original_color_at(Vector2D<size_t> coords) {
    return m_orig_image[coords.y * m_width + coords.x];
}

bool FloodableImage::already_filled(Vector2D<size_t> coords) {
    size_t idx = coords.y * m_width + coords.x;
    // FIXME: This will fail in case orig color is pure white
    return m_image[idx] == m_orig_image[idx];
}

class HighlightLayer {
public:
    HighlightLayer(size_t width, size_t height, const Pixel* orig_image);
    ~HighlightLayer();

    void highlight(Vector2D<size_t> coords);
    void clear();

    SDL_Surface* create_sdl_surface();
private:
    size_t m_width;
    size_t m_height;
    const Pixel* m_orig_image;

    // Unfortunately, the pixels needed to span 4 bytes even though this is a greyscale image
    uint8_t* m_image;

    std::unordered_set<size_t> m_highlighted;

    SDL_Surface* m_surface = nullptr;
};

HighlightLayer::HighlightLayer(size_t width, size_t height, const Pixel* orig_image)
    : m_width(width), m_height(height), m_orig_image(orig_image) {
    m_image = new uint8_t[4*m_width*m_height];
    for(size_t i=0; i<m_width*m_height; i++) {
        m_image[4*i] = 0xaa;
        m_image[4*i+1] = 0xaa;
        m_image[4*i+2] = 0xaa;
        m_image[4*i+3] = 0x00;
    }
}

HighlightLayer::~HighlightLayer() {
    if(m_surface) SDL_FreeSurface(m_surface);
    delete[] m_image;
}

void HighlightLayer::highlight(Vector2D<size_t> coords) {
    assert(m_surface);

    size_t orig_idx = coords.y * m_width + coords.x;
    if(m_highlighted.find(orig_idx) != m_highlighted.end())
        return;

    if(SDL_LockSurface(m_surface) < 0) 
        sdl_perror("SDL_LockSurface");

    for(size_t idx : m_highlighted)
        m_image[4*idx+3] = 0x00;
    m_highlighted.clear();

    std::queue<int> Q;

    auto orig_color = m_orig_image[orig_idx];
    // fmt::print("coords {} orig_color {}\n", coords, orig_color);

    auto from_coords = [this](Vector2D<int> p) -> std::optional<size_t> {
        if(p.x < 0 || p.y < 0) return {};
        if(p.x >= (int)m_width || p.y >= (int)m_height) return {};
        return p.y*m_width+p.x;
    };

    auto to_coords = [this](size_t idx) -> Vector2D<int> {
        return { (int)(idx % m_width), (int)(idx / m_width) };
    };

    auto index_has_orig_color = [this, &orig_color](size_t idx) -> bool {
        return m_orig_image[idx] == orig_color;
    };

    Q.push(coords.y*m_width + coords.x);

    while (!Q.empty()) {
        auto idx = Q.front();
        auto c = to_coords(idx);
        // fmt::print("visiting idx {} coords {}, queue size {}\n", idx, c, Q.size());
        Q.pop();

        if(m_highlighted.find(idx) != m_highlighted.end()) continue;
        m_highlighted.insert(idx);

        m_image[4*idx+3] = 0xff;

        auto left = from_coords({ c.x-1, c.y });
        auto right = from_coords({ c.x+1, c.y });
        auto top = from_coords({ c.x, c.y-1 });
        auto bottom = from_coords({ c.x, c.y+1 });

        if(left && index_has_orig_color(*left) && m_highlighted.find(*left) == m_highlighted.end())
            Q.push(*left);
        if(right && index_has_orig_color(*right) && m_highlighted.find(*right) == m_highlighted.end())
            Q.push(*right);
        if(top && index_has_orig_color(*top) && m_highlighted.find(*top) == m_highlighted.end())
            Q.push(*top);
        if(bottom && index_has_orig_color(*bottom) && m_highlighted.find(*bottom) == m_highlighted.end())
            Q.push(*bottom);
    }

    SDL_UnlockSurface(m_surface);
}

void HighlightLayer::clear() {
    for(size_t idx : m_highlighted)
        m_image[4*idx+3] = 0x00;
    m_highlighted.clear();
}

SDL_Surface* HighlightLayer::create_sdl_surface() {
    assert(!m_surface);
    m_surface = SDL_CreateRGBSurfaceFrom((void*)m_image, m_width, m_height, 
        32, 4*m_width, 
        0x000000ff, 0x0000ff00, 0x00ff0000, 0xff000000);
    if(!m_surface) sdl_perror("SDL_CreateRGBSurfaceFrom");
    
    return m_surface;
}

int main(int argc, char** argv) {
    if(argc < 2) {
        fmt::print("USAGE: {} FILENAME\n", argv[0]);
        return 1;
    }

    SDL_Window* window = nullptr;
    SDL_Surface* screen_surface = nullptr;

    if(SDL_Init(SDL_INIT_VIDEO) < 0) sdl_perror("SDL_Init");

    window = SDL_CreateWindow("Paint by Numbers", 
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 
        SCREEN_WIDTH, SCREEN_HEIGHT, 
        SDL_WINDOW_SHOWN);
    
    if(!window) sdl_perror("SDL_CreateWindow");

    int image_width = 0;
    int image_height = 0;

    // TODO: Load *.dat files separately
    Pixel* pixels = (Pixel*)stbi_load(argv[1], &image_width, &image_height, nullptr, 3);
    if(!pixels) {
        fmt::print("Invalid image file\n");
        return 1;
    }

    // TODO: assert image width and height fit into signed integers

    // TODO: Have an image class for resource management
    GaussianBlur blur(3.5f);
    blur.apply_in_place(image_width, image_height, pixels);

    // TODO: Try a better color quantization algorithm (k-means?)
    //       Or maybe try running the algorithm in LAB color space
    ColorQuantizer quant(pixels, image_width*image_height);
    // TODO: Save the image?

    quant.quantize();

    for(size_t i=0; i<(size_t)image_width*image_height; i++)
        pixels[i] = quant.color_at(i);

    ImageViewport viewport(image_width, image_height, SCREEN_WIDTH, SCREEN_HEIGHT);

    screen_surface = SDL_GetWindowSurface(window);
    uint32_t black = SDL_MapRGB(screen_surface->format, 0x00, 0x00, 0x00);

    FloodableImage displayed_image(image_width, image_height, pixels);
    HighlightLayer highlight(image_width, image_height, pixels);
    SDL_Surface* image_surface = displayed_image.create_sdl_surface();
    SDL_Surface* highlight_surface = highlight.create_sdl_surface();

    SDL_Cursor* default_cursor = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_ARROW);
    SDL_Cursor* drag_cursor = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_HAND);

    SDL_Event e;
    bool quit = false;
    while(!quit) {
        while(SDL_PollEvent(&e)) {
            if(e.type == SDL_QUIT) quit = true;

            // TODO: Have middle click do drag only
            if(e.type == SDL_MOUSEBUTTONDOWN && e.button.button == SDL_BUTTON_LEFT) {
                SDL_SetCursor(drag_cursor);
                viewport.mouse_down(e.button.x, e.button.y);
            }
            if(e.type == SDL_MOUSEBUTTONUP && e.button.button == SDL_BUTTON_LEFT) {
                // TODO: Set mouse cursor with SDL_CreateSystemCursor and SDL_SetCursor
                if(!viewport.is_dragging()) {
                    auto pos = viewport.current_location_in_imagespace();
                    if(pos) {
                        displayed_image.flood_at(*pos, displayed_image.original_color_at(*pos));
                        highlight.clear();
                    }
                }
                SDL_SetCursor(default_cursor);
                viewport.mouse_up(e.button.x, e.button.y);
            }

            if(e.type == SDL_MOUSEMOTION) {
                viewport.mouse_move(e.motion.x, e.motion.y);
                auto pos = viewport.current_location_in_imagespace();
                if(pos && !displayed_image.already_filled(*pos))
                    highlight.highlight(*pos);
            }

            if(e.type == SDL_MOUSEWHEEL && e.wheel.preciseY != 0) viewport.mouse_wheel(e.wheel.preciseY);
        }

        viewport.tick();

        SDL_FillRect(screen_surface, nullptr, black);
        SDL_Rect image_rect = viewport.image_rect(), viewport_rect = viewport.viewport_rect();
        // fmt::print("loop {} {}\n", image_rect, viewport_rect);
        SDL_BlitScaled(image_surface, &image_rect, screen_surface, &viewport_rect);
        image_rect = viewport.image_rect();
        viewport_rect = viewport.viewport_rect();
        SDL_BlitScaled(highlight_surface, &image_rect, screen_surface, &viewport_rect);
        SDL_UpdateWindowSurface(window);
    }

    stbi_image_free((void*)pixels);

    SDL_FreeCursor(default_cursor);
    SDL_FreeCursor(drag_cursor);

    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}